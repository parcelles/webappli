# Project Title

Application Web pour un SIG d’exploitation agricole Kervelique.

## Description

L'objectif est de créer une application Web pour visualiser et gérer les parcelles agricoles.

## Getting Started

### Dependencies

- Windows 10 X64 bit.
- WAPP server.
- Postgis extension for PostgreSQL.

- [leaflet](https://leafletjs.com/)
- [Bootstrap](https://getbootstrap.com/docs/4.3/getting-started/introduction/)
- [jQuery](https://jquery.com/)

## Authors

Contributors names and contact info

[@Jean-Philippe Babau](Jean-Philippe.Babau@univ-brest.fr)
[@Abderrahmen Ghadbane](mailto:Abderrahmen.Ghadbane@etudiant.univ-brest.fr)

## Version

- 0.1
  - Initial Release

## License

This project is licensed under the [gpl-3.0] License- see the [LICENSE.md](/LICENSE.md) file for details.
