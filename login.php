<?php  
require_once("data/session.php"); 
require_once("data/db_connection.php"); 

//login account query
//$str="";
if(isset($_POST['username']) && isset($_POST['password'])){

  $username=pg_escape_string($connection, $_POST['username']);
  $password=pg_escape_string($connection, $_POST['password']);

  $query="select * ";
  $query.="from users where ";
  $query.="username='{$username}' limit 1";

  $result=pg_query($connection,$query);

  //test if there is a query 
    if($result){ //success
      
      
      $row=pg_fetch_assoc($result);
      if($row){
        if(password_verify($password, $row['password']) ){
          $_SESSION["idAdmin"]=$row['id'];
          $_SESSION["usernameAdmin"]=$row['username'];
          $_SESSION["emailAdmin"]=$row['email'];
          $_SESSION["firstNameAdmin"]=$row['first_name'];
          $_SESSION["lastNameAdmin"]=$row['last_name'];
        }
        
        
      }
    }


}

 if(isset($_SESSION["idAdmin"]) && isset($_SESSION["usernameAdmin"])){
 
     header("Location: index.php");
     exit;

 }
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Se connecter</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
  </head>
  <body class="text-center">
    <form class="form-signin needs-validation" role="form" action="login.php" name="loginForm" method="POST" id="loginForm" novalidate="">
  <img class="mb-4" src="data/pictures/logo.png" alt="" height="72">
  <h1 class="h3 mb-3 font-weight-normal">Se connecter</h1>
  <label for="username" class="sr-only">Utilisateur</label>
  <input type="text" id="username" name="username" class="form-control" placeholder="Utilisateur" required>
  <label for="username" class="sr-only">Mot de passe</label>
  <input type="password" id="username" name="password" class="form-control" placeholder="Mot de passe" required>
  <div class="checkbox mb-3">
    <label>
      <input type="checkbox" value="remember-me"> Se souvenir de moi
    </label>
  </div>
  <button class="btn btn-lg btn-primary btn-block" type="submit">Se connecter</button>
  <p class="mt-5 mb-3 text-muted">&copy; <?php echo date("Y"); ?></p>
</form>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="css/bootstrap.min.css"></script>
        <!-- form validation -->
        <script src="js/validator.js"></script>

</body>
</html>
