<?php

require_once("session.php");
require_once("db_connection.php"); 
require_once("functions.php"); 

//redirect if not auth
if(!isset($_SESSION['idAdmin'])) SendToLogin();

if(isset($_POST['date']) && isset($_POST['event_type']) && isset($_POST['id']))
{
    $date= $_POST['date'];
    $event_type= $_POST['event_type'];
    $id= $_POST['id'];
    $event_param= null;

if(isset($_POST['event_param']))
    $event_param= $_POST['event_param'];

    $result = addEvent($date,$event_type,$event_param,$id);
    if(!$result){
        die("query error");

        //get this id from hidden field instead
    }
 
}
    
?>