<?php

require_once("session.php");
require_once("db_connection.php"); 
require_once("functions.php"); 

//redirect if not auth
if(!isset($_SESSION['idAdmin'])) SendToLogin();

if(isset($_POST['id']))
{
    $id= $_POST['id'];

    $result = loadParcelInfo($id);

    if(!$result){
        echo('No records found');
        die();
    }

    $row=pg_fetch_assoc($result);

    $parcelInfo = array(
        'commune'=> $row['commune'],
        'adresse'=> $row['adresse'],
        'codeCadastre'=> $row['code_cadastre'],
        'numeroCadastre'=> $row['numero_cadastre'],
        'montantAchat'=> $row['montant_achat'],
        'dateAutorisation'=> $row['date_autorisation'],
        'releveMsa'=> $row['releve_msa'],

        'codeMae'=> $row['code_mae'],
        'parcelType'=> $row['type'],
        'nomProprietaire'=> $row['nom_proprietaire'],
        'nomLocataire'=> $row['nom_locataire'],
        'demandePac'=> $row['demande_pac'],
        'typeAnimaux'=> $row['type_animaux'],
        'nombreAnimaux'=> $row['nombre_animaux'],
        'montantLoyer'=> $row['montant_loyer'],

        'nomShapefile'=> $row['nom_shapefile'],
    );

    echo $jsonformat=json_encode($parcelInfo);
}
    
?>