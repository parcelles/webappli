<?php

require_once("session.php");
require_once("db_connection.php"); 
require_once("functions.php"); 

//redirect if not auth
if(!isset($_SESSION['idAdmin'])) SendToLogin();

if(isset($_POST['id']))
{
    $id= $_POST['id'];

    $result = showEventsByIdParcel($id);

    if(!$result){
        echo('No records found');
        die();
    }

    $articles="";
    while($row=pg_fetch_assoc($result)){


    $articles.='<tr>';
    $articles.='<td class="d-none">';
    $articles.=$row['id'];
    $articles.='</td>';
    $articles.='<td class="d-none">';
    $articles.=$row['id_parcelle'];
    $articles.='</td>';  
    $articles.='<td>';
    $articles.=$row['type'];
    $articles.='</td>';
    $articles.='<td>';
    $articles.=$row['parameter'];
    $articles.='</td>';
    $articles.='<td>';
    $articles.=$row['date'];
    $articles.='</td>';
    $articles.='<td><button type="button" class="editEventBtn btn btn-success btn-sm"><span data-feather="edit"></button></td>';
    $articles.='<td><button type="button" class="deleteEventBtn btn btn-danger btn-sm"><span data-feather="trash-2"></button></td>';
    $articles.='<tr>';
    

    }

    echo($articles);
 
}
    
?>