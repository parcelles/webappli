<?php

require_once("session.php");
require_once("db_connection.php"); 
require_once("functions.php"); 

//redirect if not auth
if(!isset($_SESSION['idAdmin'])) SendToLogin();

if(isset($_POST['parcelsIdsModify']))
{
    $id= $_POST['parcelsIdsModify'];

    $commune=$adresse=$code_cadastre=$numero_cadastre=
    $montant_achat=$date_autorisation=$releve_msa=$code_mae=$type=
    $nom_proprietaire=$nom_locataire=$demande_pac=$type_animaux=$nombre_animaux=$montant_loyer
    =$nom_shapefile= null;

    if(isset($_POST['editParcelCommune'])) $commune= $_POST['editParcelCommune'];
    if(isset($_POST['editParcelAdresse'])) $adresse= $_POST['editParcelAdresse'];
    if(isset($_POST['editParcelCodeCadastre'])) $code_cadastre= $_POST['editParcelCodeCadastre'];
    if(isset($_POST['editParcelNumCadastre'])) $numero_cadastre= $_POST['editParcelNumCadastre'];
    if(isset($_POST['editParcelMontantAchat'])) $montant_achat= $_POST['editParcelMontantAchat'];
    if(isset($_POST['editParcelAutorisationDate'])) $date_autorisation= $_POST['editParcelAutorisationDate'];
    if(isset($_POST['editParcelMSA'])) $releve_msa= $_POST['editParcelMSA'];

    if(isset($_POST['editParcelCodeMae'])) $code_mae= $_POST['editParcelCodeMae'];
    if(isset($_POST['editParcelType'])) $type= $_POST['editParcelType'];
    if(isset($_POST['editParcelNomProprietaire'])) $nom_proprietaire= $_POST['editParcelNomProprietaire'];
    if(isset($_POST['editParcelNomLocataire'])) $nom_locataire= $_POST['editParcelNomLocataire'];
    if(isset($_POST['editParcelDemandePac'])) $demande_pac= $_POST['editParcelDemandePac'];
    if(isset($_POST['editParcelTypeAnimaux'])) $type_animaux= $_POST['editParcelTypeAnimaux'];
    if(isset($_POST['editParcelNombreAnimaux'])) $nombre_animaux= $_POST['editParcelNombreAnimaux'];
    if(isset($_POST['editParcelMontantLoyer'])) $montant_loyer= $_POST['editParcelMontantLoyer'];

    if(isset($_POST['editParcelNomShapefile'])) $nom_shapefile= $_POST['editParcelNomShapefile'];

    $result = editParcel($id,$commune,$adresse,$code_cadastre,$numero_cadastre,$montant_achat,$date_autorisation,$releve_msa,$code_mae,$type,
    $nom_proprietaire,$nom_locataire,$demande_pac,$type_animaux,$nombre_animaux,$montant_loyer,$nom_shapefile);
    if(!$result){
        //die("query error");
        echo "0";
    }
    echo "1";
 
}

    
?>