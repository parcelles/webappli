<?php

function GetUserById($id)
{
    global $connection; 

    $query="select * ";
    $query.="from users ";
    $query.="where id = '{$id}' ";
    $query.="limit 1";

    $result=pg_query($connection,$query);
    //test if there is a query 
    
    return $result;   
}

function EditUserPassword($id,$password)
{
    global $connection; 
    $password = password_hash($password, PASSWORD_DEFAULT);

    $query="UPDATE users ";
    $query.="SET password = '{$password}' ";
    $query.="WHERE id = {$id}";
    $result=pg_query($connection,$query);
    
    //test if there is a query 
    
    return $result;   
}

function EditUserInfo($id, $username, $email,$firstName,$lastName)
{
    global $connection; 

    $query="UPDATE users ";
    $query.="SET username = '{$username}', ";
    $query.="email = '{$email}', ";
    $query.="first_name = '{$firstName}', ";
    $query.="last_name = '{$lastName}' ";
    $query.="WHERE id = {$id}";

    echo($query);

    $result=pg_query($connection,$query);
    
    //test if there is a query 
    
    return $result;   
}

function addEvent($date,$event_type,$event_param, $id){
    global $connection; 
        
    $date =implode("-", array_reverse(explode("/", $date)));

    $query="INSERT INTO evenements (";
    $query.="date, type, parameter, id_parcelle";
    $query.=") VALUES (";
    $query.=" '{$date}','{$event_type}','{$event_param}','{$id}' ";
    $query.=")";
    //echo $query;
    //test if there is a query 
    $result=pg_query($connection,$query);

    if($result){
        return true;
    }
    else{
        return false;
    }
    
}


function editEvent($date,$event_type,$event_param, $idEvent){
    global $connection; 
        
    $date =implode("-", array_reverse(explode("/", $date)));

    $query="UPDATE evenements ";
    $query.="SET date='{$date}',";
    $query.="type='{$event_type}',";
    $query.="parameter='{$event_param}' ";
    $query.="WHERE id='{$idEvent}'";
    //echo $query;
    //test if there is a query 
    $result=pg_query($connection,$query);

    if($result){
        return true;
    }
    else{
        return false;
    }
    
}

function deleteEvent($eventId){
    global $connection; 
        
    $query="DELETE FROM evenements ";
    $query.="WHERE id={$eventId} ";
    //echo $query;
    //test if there is a query 
    $result=pg_query($connection,$query);

    if($result){
        return true;
    }
    else{
        return false;
    }
    
}

function showEvents()
{
    global $connection; 

    $query="select * ";
    $query.="from evenements";
    
    $result=pg_query($connection,$query);
    //test if there is a query 
    
    return $result;   
}

function showEventsByIdParcel($id)
{
    global $connection; 

    $query="select * ";
    $query.="from evenements ";
    $query.="where id_parcelle='{$id}' ";
    $query.="ORDER BY date DESC";

    $result=pg_query($connection,$query);
    //test if there is a query 
    
    return $result;   
}

function getParcelsIds(){
    global $connection; 

    $query="select id ";
    $query.="from parcelles ";
    $query.="ORDER BY id::int ASC";

    $result=pg_query($connection,$query);
    //test if there is a query 
    
    return $result;   

}

function loadParcelInfo($id)
{
    global $connection; 

    $query="select * ";
    $query.="from parcelles ";
    $query.="where id='{$id}' ";
    $query.="LIMIT 1";

    $result=pg_query($connection,$query);
    //test if there is a query 
    
    return $result;   
}

function editParcel($id,$commune,$adresse,$code_cadastre,$numero_cadastre,$montant_achat,$date_autorisation,$releve_msa,$code_mae,$type,
$nom_proprietaire,$nom_locataire,$demande_pac,$type_animaux,$nombre_animaux,$montant_loyer,$nom_shapefile){
    global $connection; 
        
    if(isset($date_autorisation)) $date_autorisation =implode("-", array_reverse(explode("/", $date_autorisation)));
    else $date_autorisation=NULL;
    if(!$nombre_animaux) $nombre_animaux ='NULL';

    $query="UPDATE parcelles SET ";
    $query.="commune='{$commune}' ";
    if($date_autorisation=="") $query.=",date_autorisation=NULL ";
    else $query.=",date_autorisation='{$date_autorisation}' ";
    $query.=",adresse='{$adresse}' ";
    $query.=",code_cadastre='{$code_cadastre}' ";
    $query.=",numero_cadastre='{$numero_cadastre}' ";
    $query.=",montant_achat='{$montant_achat}' ";
    
    $query.=",releve_msa='{$releve_msa}' ";

    $query.=",code_mae='{$code_mae}' ";
    $query.=",type='{$type}' ";
    $query.=",nom_proprietaire='{$nom_proprietaire}' ";
    $query.=",nom_locataire='{$nom_locataire}' ";
    $query.=",demande_pac='{$demande_pac}' ";
    $query.=",type_animaux='{$type_animaux}' ";
    $query.=",nombre_animaux={$nombre_animaux} ";
    $query.=",montant_loyer='{$montant_loyer}' ";

    $query.=",nom_shapefile='{$nom_shapefile}' ";

    $query.="WHERE id='{$id}'";

    //echo $query;

    //test if there is a query 
    $result=pg_query($connection,$query);

    if($result){
        //return true;
        return $query;
    }
    else{
        //return false;
        return $query;
    }
    
}

function deleteParcels(){
    global $connection; 
        
    $query="TRUNCATE parcelles, evenements";
    //test if there is a query 
    $result1=pg_query($connection,$query);

    $query2="ALTER SEQUENCE parcelle_id_seq RESTART WITH 1";
    //test if there is a query 
    $result2=pg_query($connection,$query2);
    $query3="ALTER SEQUENCE evenements_id_seq RESTART WITH 1";
    //test if there is a query 
    $result3=pg_query($connection,$query3);

    if($result1 && $result2 && $result3){
        return true;
    }
    else{
        return false;
    }
    
}

function SendToLogin(){
    header("Location: login.php");
	exit;
}


?>