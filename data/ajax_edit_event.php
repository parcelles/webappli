<?php

require_once("session.php");
require_once("db_connection.php"); 
require_once("functions.php"); 

//redirect if not auth
if(!isset($_SESSION['idAdmin'])) SendToLogin();

if(isset($_POST['editEventDate']) && isset($_POST['editEventType']) && isset($_POST['editEventId']))
{
    $date= $_POST['editEventDate'];
    $event_type= $_POST['editEventType'];
    $id= $_POST['editEventId'];
    $event_param= null;

if(isset($_POST['editEventParam']))
    $event_param= $_POST['editEventParam'];

    $result = editEvent($date,$event_type,$event_param,$id);
    if(!$result){
        die("query error");

        //get this id from hidden field instead
    }
 
}
    
?>