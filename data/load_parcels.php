<?php 

//1-create connection to the database
require_once("session.php");
require_once("db_connection.php"); 
require_once("functions.php"); 

//redirect if not auth
if(!isset($_SESSION['idAdmin'])) SendToLogin();

ini_set('memory_limit', '1024M'); // or you could use 1G

$query="select id, ST_AsGeoJSON(geom,6) AS geojson, commune,adresse,ST_Area(geom,false) AS surface,numero_cadastre, montant_achat, date_autorisation, releve_msa, 
type, type_animaux, nombre_animaux, code_mae, nom_proprietaire, nom_locataire, demande_pac, montant_loyer ";
$query.="from parcelles ";
//$query.="LIMIT 50 ";

$result=pg_query($connection,$query);
//test if there is a query 
if(!$result){
	die("query error");
}

$features = [];
while($row=pg_fetch_assoc($result)){

    unset($row['geom']);

    $geometry = $row['geojson'] = json_decode($row['geojson']);
    unset($row['geojson']);
    $feature = ["type" => "Feature", "geometry" => $geometry, "properties" => $row];
    array_push($features,$feature);
    
    //echo var_dump($row);
}
$featureCollection = ["type" => "FeatureCollection", "features" => $features];
echo json_encode($featureCollection);




?>