<?php  
require_once("data/session.php"); 
require_once("data/db_connection.php"); 
require_once("data/functions.php"); 

//redirect if not auth
if(!isset($_SESSION['idAdmin'])) SendToLogin();

//login account query
$str="";
if(isset($_POST['oldPassword']) && isset($_POST['newPassword']) ){

  $oldPassword=pg_escape_string($connection, $_POST['oldPassword']);
  $newPassword=pg_escape_string($connection, $_POST['newPassword']);
  $id="";
  if(isset($_SESSION["idAdmin"])) $id= $_SESSION["idAdmin"];

  //get user by username
	$result = GetUserById($id);
	if(!$result){
		die("query error");
	}

    $row=pg_fetch_assoc($result);
    if($row){

		//check if current password match
		if(password_verify($oldPassword, $row['password'])){
        
            if($newPassword != ""){
                EditUserPassword($id,$newPassword);
                $str='<p class="label label-sm label-success">Votre mot de passe a été modifié avec succès</p>';
            }
		}

	}
    else{
		$str='<p class="label label-sm label-danger">Query error</p>';
    }
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="data/pictures/favicon.ico" />

    <title>Changer le mot de passe</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet" />
  </head>

  <body>
    <!-- navbar -->
    <?php include 'nav.php'; ?>

    <div class="container-fluid">
      <div class="row">
        <!-- sidebar -->
        <?php include 'sidebar.php'; ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"
          >
            <h1 class="h2">Changer le mot de passe</h1>
          </div>

          <div class="col-md-8 order-md-1">
            <form class="needs-validation" novalidate="" action="change_password.php" method="POST" role="form">

                      <!-- current password -->
            <div class="mb-3">
                <label for="oldPassword"
                  > Mot de pass actuel <span class="text-muted"></span></label
                >
                <input
                  type="password"
                  class="form-control"
                  id="oldPassword"
                  name="oldPassword"
                  required=""
                />
                <div class="invalid-feedback">
                  Please enter your current Password.
                </div>
              </div>

                      <!-- new password -->
              <div class="mb-3">
                <label for="newPassword">
                Nouveau mot de passe <span class="text-muted"></span></label
                >
                <input
                  type="password"
                  class="form-control"
                  id="newPassword"
                  name="newPassword"
                  required=""
                />
                <div class="invalid-feedback">
                  Please enter a new password.
                </div>
              </div>

                      <!-- confirm password -->
              <div class="mb-3">
                <label for="confirmPassword">
                Confirmation de mot de pass <span class="text-muted"></span></label
                >
                <input
                  type="password"
                  class="form-control"
                  id="confirmPassword"
                  required=""
                />
                <div class="invalid-feedback">
                  Please retype your new password.
                </div>
              </div>

              <div class="form-group row">
												<div class="col-md-5 offset-3">
													<?php echo $str;?>
												</div>
											</div>

              <hr class="mb-4" />
              <button class="btn btn-primary btn-lg" type="submit">
                Sauvgarder
              </button>
            </form>
            <br />
          </div>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="css/bootstrap.min.css"></script>

    <!-- Icons -->
    <script src="js/feather.min.js"></script>
    <script>
      feather.replace();
    </script>

            <!-- form validation -->
    <script src="js/validator.js"></script>

  </body>
</html>
