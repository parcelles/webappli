var mymap = L.map("mapid", {
  //disable zoom buttons (we will add them manually)
  zoomControl: false
});

var OpenStreetMap_Org = L.tileLayer(
  "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
  {
    maxZoom: 19,
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  }
);
var OpenStreetMap_BZH = L.tileLayer(
  "https://tile.openstreetmap.bzh/br/{z}/{x}/{y}.png",
  {
    maxZoom: 19,
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Tiles courtesy of <a href="http://www.openstreetmap.bzh/" target="_blank">Breton OpenStreetMap Team</a>'
  }
);

var OpenStreetMap_France = L.tileLayer(
  "https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png",
  {
    maxZoom: 20,
    attribution:
      '&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  }
).addTo(mymap);

//add zoom buttons (manually)
L.control
  .zoom({
    position: "topright"
  })
  .addTo(mymap);

//click on parcel
function selectParcel(e) {
  //set the id of parcel in the select input when the user clicks on it
  $("#parcelsIdsModify").val(e.target.feature.properties.id);
  $("#parcelsIdsModify").change();

  $("#parcels").val(e.target.feature.properties.id);
}

//geoJson
//var ParcellesGeojsonLayer;

//using ajax:
/*     $.ajax({ 
                url:'data/load_parcelles.php', 
            success: function(response){
                geojsonData = JSON.parse(response);
                ParcellesGeojsonLayer = L.geoJSON(geojsonData,{
                onEachFeature: onEachFeature,
                style: style
            }).addTo(mymap);
            },
            error: function(xhr,status,error){
                alert("ERROR:"+error);
            }
        
            }); */

//using ajax syncronous
var ParcellesGeojsonLayer;
$.ajax({
  url: "data/load_parcels.php",
  async: false,
  success: function(response) {
    geojsonData = JSON.parse(response);
    ParcellesGeojsonLayer = L.geoJSON(geojsonData, {
      onEachFeature: onEachFeature,
      style: style
    });

    //
  },
  error: function(xhr, status, error) {
    alert("ERROR:" + error);
  }
});

///////////////
function onEachFeature(feature, layer) {
  // does this feature have a property named popupContent?
  //if (feature.properties && feature.properties.id) {

  //bind click to select parcel
  layer.on({
    click: selectParcel
  });

  var popupContent =
    '<table>\
                            <tr>\
                                <th colspan="2">' +
    "Parcelle N: " +
    String(feature.properties["id"]) +
    '</th>\
                            </tr>\
                            <tr>\
                                <td colspan="2">' +
    "type : " +
    String(feature.properties["type"]) +
    '</td>\
                            </tr>\
                            <tr>\
                                <td colspan="2">' +
    "surface : " +
    String(parseFloat(feature.properties["surface"]).toFixed(2)) +
    ' M²</td>\
                            </tr>\
                            <tr>\
                                <td colspan="2">' +
    "commune : " +
    String(feature.properties["commune"]) +
    '</td>\
                            </tr>\
                            <tr>\
                                <td colspan="2">' +
    "type animaux : " +
    String(feature.properties["type_animaux"]) +
    '</td>\
                            </tr>\
                            <tr>\
                                <td colspan="2">' +
    "nombre animaux: " +
    String(feature.properties["nombre_animaux"]) +
    '</td>\
                            </tr>\
                            <tr>\
                                <td colspan="2">' +
    "code MAE : " +
    String(feature.properties["code_mae"]) +
    '</td>\
                            </tr>\
                            <tr>\
                                <td colspan="2">' +
    "montant achat : " +
    String(feature.properties["montant_achat"]) +
    '</td>\
                            </tr>\
                            <tr>\
                                <td colspan="2">' +
    "nom proprietaire : " +
    String(feature.properties["nom_proprietaire"]) +
    '</td>\
                            </tr>\
                            <tr>\
                                <td colspan="2">' +
    "nom locataire : " +
    String(feature.properties["nom_locataire"]) +
    '</td>\
                            </tr>\
                            <tr>\
                                <td colspan="2">' +
    "demande pac : " +
    String(feature.properties["demande_pac"]) +
    '</td>\
                            </tr>\
                            <tr>\
                                <td colspan="2">' +
    "montant loyer : " +
    String(feature.properties["montant_loyer"]) +
    '</td>\
                            </tr>\
                            <tr>\
                                <td colspan="2">' +
    "date autorisation : " +
    String(feature.properties["date_autorisation"]) +
    "</td>\
                            </tr>\
                        </table>";

  layer.bindPopup(popupContent, { maxHeight: 400 });
  //}
}

///// color function

function getColorAnimal(type_animaux) {
  if (type_animaux == "vache") return "red";
  else if (type_animaux == "genisse") return "yellow";
  else return "grey";
  //else return '#FFEDA0';
}

function getColorMae(code_mae) {
  if (code_mae == "bocage") return "teal";
  else if (code_mae == "SPE12") return "aqua";
  else return "grey";
}

function getColorType(type) {
  if (type == "prairie naturelle") return "yellow";
  else if (type == "prairie humide") return "orange";
  else return "grey";
}

//styling function
function style(feature) {
  return {
    fillColor: getColorType(feature.properties.type),
    weight: 2,
    opacity: 1,
    color: "black",
    dashArray: "3",
    fillOpacity: 0.7
  };
}

//legends
var typeLegend = L.control({ position: "bottomright" });

typeLegend.onAdd = function(mymap) {
  var div = L.DomUtil.create("div", "info legend"),
    grades = ["prairie naturelle", "prairie humide", "NA"],
    labels = [];

  // loop through our density intervals and generate a label with a colored square for each interval
  for (var i = 0; i < grades.length; i++) {
    div.innerHTML +=
      '<i style="background:' +
      getColorType(grades[i]) +
      '"></i>' +
      grades[i] +
      "<br>";
  }

  return div;
};

//legend area
var areaLegend = L.control({ position: "bottomright" });

areaLegend.onAdd = function(mymap) {
  var div = L.DomUtil.create("div", "info legend");

  // loop through our density intervals and generate a label with a colored square for each interval

  div.innerHTML +=
    '<i style="background:teal"></i>bocage<br><i style="background:aqua"></i>SPE12<br><i style="background:grey"></i>NA<br>';

  return div;
};

//legend value
var valueLegend = L.control({ position: "bottomright" });

valueLegend.onAdd = function(mymap) {
  var div = L.DomUtil.create("div", "info legend");

  // loop through our density intervals and generate a label with a colored square for each interval

  div.innerHTML +=
    '<i style="background:red"></i>vache<br><i style="background:yellow"></i>génisse<br><i style="background:grey"></i>NA<br>';

  return div;
};

typeLegend.addTo(mymap);
currentLegend = typeLegend;

//add layers

var baseLayers = {
  "Carte monde": OpenStreetMap_Org,
  "Carte Bretagne": OpenStreetMap_BZH,
  "Carte France": OpenStreetMap_France
};

var overlayLayers = {
  parcelles: ParcellesGeojsonLayer
};

L.control.layers(baseLayers, overlayLayers).addTo(mymap);

//show parcels overlay layer
ParcellesGeojsonLayer.addTo(mymap);

//fit in screen
mymap.fitBounds(ParcellesGeojsonLayer.getBounds());
//add sidebar
var sidebar = L.control.sidebar("sidebar").addTo(mymap);

//click on radio to change color
$(document).ready(function() {
  $('input[name="colorParcelles"]').change(function() {
    if ($("#areaRadio").prop("checked")) {
      //alert('Option area is checked!');
      ParcellesGeojsonLayer.eachLayer(function(layer) {
        layer.setStyle({
          fillColor: getColorMae(layer.feature.properties.code_mae)
        });

        //change legend
        mymap.removeControl(currentLegend);
        currentLegend = areaLegend;
        areaLegend.addTo(mymap);
      });
    } else {
      //alert('Option area is unchecked!');
    }

    if ($("#valueRadio").prop("checked")) {
      //alert('Option area is checked!');
      ParcellesGeojsonLayer.eachLayer(function(layer) {
        layer.setStyle({
          fillColor: getColorAnimal(layer.feature.properties.type_animaux)
        });

        //change legend
        mymap.removeControl(currentLegend);
        currentLegend = valueLegend;
        valueLegend.addTo(mymap);
      });
    } else {
      //alert('Option value is unchecked!');
    }

    if ($("#typeRadio").prop("checked")) {
      //alert('Option type is checked!');
      //ParcellesGeojsonLayer.setStyle({fillColor : getColor(feature.properties.area)});

      ParcellesGeojsonLayer.eachLayer(function(layer) {
        //console.log(layer.feature.properties.type);

        layer.setStyle({
          fillColor: getColorType(layer.feature.properties.type)
        });

        //change legend
        mymap.removeControl(currentLegend);
        currentLegend = typeLegend;
        typeLegend.addTo(mymap);
      });
    } else {
      //alert('Option type is unchecked!');
    }
  });
});
