//fill form to edit parcel
$("#parcelsIdsModify").on("change", function () {
  $.ajax({
    type: "POST",
    url: "data/load_parcel_info.php",
    cache: false,
    data: { id: $("#parcelsIdsModify").val() },
    beforeSend: function () {
      $("#waitIndicatorModifyParcel").show();
      $("#parcelEditOkMsg").hide();
      $("#parcelEditFailMsg").hide();
    },
    complete: function () {
      $("#waitIndicatorModifyParcel").hide();
    },
    success: function (response) {
      var json = $.parseJSON(response);
      //fill inputs
      $("#editParcelCommune").val(json.commune);
      $("#editParcelAdresse").val(json.adresse);
      $("#editParcelCodeCadastre").val(json.codeCadastre);
      $("#editParcelNumCadastre").val(json.numeroCadastre);
      $("#editParcelMontantAchat").val(json.montantAchat);
      $("#editParcelAutorisationDate").val(json.dateAutorisation);

      if (json.releveMsa == "oui")
        $("#editParcelMSARadio1").prop("checked", true);
      if (json.releveMsa == "non")
        $("#editParcelMSARadio2").prop("checked", true);
      if (json.releveMsa != "non" && json.releveMsa != "oui")
        $("#editParcelMSARadio2, #editParcelMSARadio1").prop("checked", false);

      if (json.codeMae == "SPE12")
        $("#editParcelCodeMaeRadio1").prop("checked", true);
      if (json.codeMae == "bocage")
        $("#editParcelCodeMaeRadio2").prop("checked", true);
      if (json.codeMae != "SPE12" && json.codeMae != "bocage")
        $("#editParcelCodeMaeRadio2, #editParcelCodeMaeRadio1").prop(
          "checked",
          false
        );

      if (json.parcelType == "prairie naturelle")
        $("#editParcelTypeRadio1").prop("checked", true);
      if (json.parcelType == "prairie humide")
        $("#editParcelTypeRadio2").prop("checked", true);
      if (
        json.parcelType != "prairie naturelle" &&
        json.parcelType != "prairie humide"
      )
        $("#editParcelTypeRadio2, #editParcelTypeRadio1").prop(
          "checked",
          false
        );

      if (json.typeAnimaux == "vache")
        $("#editParcelTypeAnimauxRadio1").prop("checked", true);
      if (json.typeAnimaux == "genisse")
        $("#editParcelTypeAnimauxRadio2").prop("checked", true);
      if (json.typeAnimaux != "vache" && json.typeAnimaux != "genisse")
        $("#editParcelTypeAnimauxRadio2, #editParcelTypeAnimauxRadio1").prop(
          "checked",
          false
        );

      if (json.demandePac == "oui")
        $("#editParcelDemandePacRadio1").prop("checked", true);
      if (json.demandePac == "non")
        $("#editParcelDemandePacRadio2").prop("checked", true);
      if (json.demandePac != "non" && json.demandePac != "oui")
        $("#editParcelDemandePacRadio2, #editParcelDemandePacRadio1").prop(
          "checked",
          false
        );

      $("#editParcelNomProprietaire").val(json.nomProprietaire);
      $("#editParcelNomLocataire").val(json.nomLocataire);
      $("#editParcelNombreAnimaux").val(json.nombreAnimaux);
      $("#editParcelMontantLoyer").val(json.montantLoyer);

      $("#editParcelNomShapefile").val(json.nomShapefile);
    },
    error: function () {
      //alert("Error");
      console.log(response);
    },
  });
});

//edit parcel ajax
$("#editParcelForm").submit(function (event) {
  event.preventDefault();

  $.ajax({
    type: "POST",
    url: "data/ajax_edit_parcel.php",
    cache: false,
    data: $("form#editParcelForm").serialize(),
    beforeSend: function () {
      $("#editParcelFormBtn").addClass("disabled");
    },
    complete: function () {
      $("#editParcelFormBtn").removeClass("disabled");
    },
    success: function (response) {
      if (response === "1") $("#parcelEditOkMsg").show();
      //console.log(response);
      else $("#parcelEditFailMsg").show();
    },
    error: function () {
      //alert("Error");
      console.log(response);
    },
  });
});

//show event table
$("#showEventsBtn").click(function (e) {
  e.preventDefault();
  //$('#eventsTable').show();
  //show events for specific parcel
  $.ajax({
    type: "POST",
    url: "data/load_events.php",
    cache: false,
    data: { id: $("#parcels").val() },
    beforeSend: function () {
      $("#waitIndicatorEvent").show();
    },
    complete: function () {
      $("#waitIndicatorEvent").hide();
    },
    success: function (response) {
      $("#eventsBody").html(response);
      //show icons
      feather.replace();
      console.log(response);
    },
    error: function () {
      //alert("Error");
      console.log(response);
    },
  });
});

//new event for parcel (open modal)
$("#addEventBtn").on("click", function () {
  const parcelId = $("#parcels").val();
  //add parcel id to hidden field
  $('input[name="id"]').val(parcelId);
  $("#addEventForm")
    .find(
      "input[type=text], input[type=password], input[type=number], input[type=email],input[type=date], textarea"
    )
    .val("");
  $("#addEventModalCenterTitle").text(
    "Ajouter un événement (parcelle " + parcelId + ")"
  );

  $("#addEventModalCenter").modal("show");
});

//disable parameters when selecting events
$("#event_type").on("change", function () {
  if (this.value == "fauche" || this.value == "semis") {
    $("#event_param").val("");
    $("#event_param").attr("disabled", "disabled");
  } else {
    $("#event_param").removeAttr("disabled");
  }
});

$("#editEventType").on("change", function () {
  if (this.value == "fauche" || this.value == "semis") {
    $("#editEventParam").val("");
    $("#editEventParam").attr("disabled", "disabled");
  } else {
    $("#editEventParam").removeAttr("disabled");
  }
});

//edit event for parcel (open modal)
$("#eventsTable").on("click", ".editEventBtn", function () {
  //const parcelId = $('#parcels').val();
  //$('input[name="id"]').val(parcelId);
  //$('input[name="id"]').val(parcelId);
  //get data from row
  const eventId = $(this).parentsUntil("tr").siblings().eq(0).html();
  const parcelId = $(this).parentsUntil("tr").siblings().eq(1).html();
  const type = $(this).parentsUntil("tr").siblings().eq(2).html();
  const param = $(this).parentsUntil("tr").siblings().eq(3).html();
  const date = $(this).parentsUntil("tr").siblings().eq(4).html();

  $("#addEventForm")
    .find(
      "input[type=text], input[type=password], input[type=number], input[type=email],input[type=date], textarea"
    )
    .val("");

  //fill inputs
  $("#editEventId").val(eventId);
  $("#editEventDate").val(date);
  $("#editEventType").val(type);
  $("#editEventParam").val(param);

  //$("#errorMsg").hide();
  $("#editEventModalCenterTitle").text(
    "Modifier un événement (N° " + eventId + ")"
  );

  //verify if param input disabled
  if (
    $("#editEventType").val() == "fauche" ||
    $("#event_type").val() == "semis"
  ) {
    $("#editEventParam").val("");
    $("#editEventParam").attr("disabled", "disabled");
  } else {
    $("#editEventParam").removeAttr("disabled");
  }
  $("#editEventModalCenter").modal("show");
});

//delete event confirm (open modal)
$("#eventsTable").on("click", ".deleteEventBtn", function () {
  //const parcelId = $('#parcels').val();
  //$('input[name="id"]').val(parcelId);
  //$('input[name="id"]').val(parcelId);
  //get data from row
  const eventId = $(this).parentsUntil("tr").siblings().eq(0).html();

  //fill inputs
  $("#deleteEventIdInput").val("");
  $("#deleteEventIdInput").val(eventId);

  $("#deleteEventConfirmModalTitle").text(
    "Supprimer l`événement (N° " + eventId + ")"
  );
  $("#deleteEventConfirmModal").modal("show");
});

//add event ajax
$("#addEventForm").submit(function (event) {
  event.preventDefault();

  $.ajax({
    type: "POST",
    url: "data/ajax_add_event.php",
    cache: false,
    data: $("form#addEventForm").serialize(),
    success: function (response) {
      $("#addEventModalCenter").modal("hide");
      $("#showEventsBtn").click();
      console.log(response);
    },
    error: function () {
      //alert("Error");
      console.log(response);
    },
  });
});

//edit event ajax
$("#editEventForm").submit(function (event) {
  event.preventDefault();

  $.ajax({
    type: "POST",
    url: "data/ajax_edit_event.php",
    cache: false,
    data: $("form#editEventForm").serialize(),
    success: function (response) {
      $("#editEventModalCenter").modal("hide");
      $("#showEventsBtn").click();
      console.log(response);
    },
    error: function () {
      //alert("Error");
      console.log(response);
    },
  });
});

//delete event ajax
$("#deleteEventForm").submit(function (event) {
  event.preventDefault();

  $.ajax({
    type: "POST",
    url: "data/ajax_delete_event.php",
    cache: false,
    data: { deleteEventId: $("#deleteEventIdInput").val() },
    success: function (response) {
      $("#deleteEventConfirmModal").modal("hide");
      $("#showEventsBtn").click();
      console.log(response);
    },
    error: function () {
      //alert("Error");
      console.log(response);
    },
  });
});
