<?php  
require_once("data/session.php"); 
require_once("data/db_connection.php"); 
require_once("data/functions.php"); 

//redirect if not auth
if(!isset($_SESSION['idAdmin'])) SendToLogin();


$str="";
if(isset($_POST['submit']) && isset($_SESSION['idAdmin'])){
 
 echo "submit is set!";

  //delete all parcels and events
	$result = deleteParcels();
	if($result){
	
                $str='<div class="alert alert-success" role="alert">
                Tous les parcelles et les événements a été supprimé avec succès.
              </div>';
		}
    else{
		$str='<div class="alert alert-danger" role="alert">Query error.</div>';
    }
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="data/pictures/favicon.ico" />

    <title>Supprimer les parcelles</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet" />
  </head>

  <body>

    <!-- navbar -->
    <?php include 'nav.php'; ?>

    <div class="container-fluid">
      <div class="row">
        <!-- sidebar -->
        <?php include 'sidebar.php'; ?>

        

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"
          >
            <h1 class="h2">Supprimer tous les parcelles et les événements</h1>
          </div>

          <div class="col-md-8 order-md-1">

          <div class="form-group row">
												<div class="col-md-12">
													<?php echo $str;?>
												</div>
						</div>
            
              <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#deleteParcelsConfirmModal">
                Supprimer
              </button>

          </div>
        </main>
      </div>
    </div>

     <!-- delete Parcels confirm model starts -->     

 <div class="modal fade" id="deleteParcelsConfirmModal" tabindex="-1" role="dialog" aria-labelledby="deleteParcelsConfirmModalTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteParcelsConfirmModalTitle">Supprimer tous les parcelles et les événements</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Êtes-vous sûr?</p>
        <form id="deleteParcelsForm" name="deleteParcelsForm" action="remove_parcels.php" method="POST" role="form">
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Non</button>
        <input type="submit" name="submit" class="btn btn-success" value="Oui">
      </div>
      </form>
    </div>
  </div>
</div>
 <!-- delete Parcels confirm model ends -->    

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
       <script src="js/bootstrap.min.js"></script>
    <!-- Icons -->
    <script src="js/feather.min.js"></script>
    <script>
      feather.replace();
    </script>

  </body>
</html>
