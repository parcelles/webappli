<?php 
$url = $_SERVER['REQUEST_URI'];

$dashboardActive="";$changePasswordActive="";$usersActive="";$RemoveParcelsActive="";

if (strpos($url,'dashboard') !== false) {
    $dashboardActive='active';
}
if (strpos($url,'change_password') !== false) {
    $changePasswordActive='active';
}
if (strpos($url,'users') !== false) {
    $usersActive='active';
}
if (strpos($url,'remove_parcels') !== false) {
  $RemoveParcelsActive='active';
}

?>

<nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <img
                  src="data/pictures/user.png"
                  alt=""
                  width="100"
                  class="rounded mx-auto d-block"
                />
                <div class="text-center">
                <span ><?php if(isset($_SESSION["usernameAdmin"])) echo $_SESSION["usernameAdmin"]; ?></span>
                </div>
                <br />
              </li>
              <li class="nav-item">
                <a class="nav-link <?php echo $dashboardActive?>" href="dashboard.php">
                  <span data-feather="user"></span>
                  Données personnelles <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link <?php echo $changePasswordActive?>" href="change_password.php">
                  <span data-feather="key"></span>
                  Changer le mots de passe
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link <?php echo $RemoveParcelsActive?>" href="remove_parcels.php">
                  <span data-feather="trash-2"></span>
                  Supprimer les parcelles
                </a>
              </li>
            </ul>
          </div>
        </nav>