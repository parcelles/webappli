<?php 
require_once("data/session.php"); 
require_once("data/db_connection.php"); 
require_once("data/functions.php"); 

if(!isset($_SESSION['idAdmin'])) SendToLogin();

$ParcelsIdsResult = getParcelsIds();

$parcelsIds="";
$parcelsIds.='<select class="custom-select" id="parcels">';
$parcelsIdsModify="";
$parcelsIdsModify.='<select class="custom-select" id="parcelsIdsModify" name="parcelsIdsModify">';
$parcelsIdsModify.='<option hidden disabled selected value></option>';

while($row=pg_fetch_assoc($ParcelsIdsResult)){
  
  $parcelsIds.='<option value="'.$row['id'].'">';
  $parcelsIds.=$row['id'];
  $parcelsIds.='</option>';

  $parcelsIdsModify.='<option value="'.$row['id'].'">';
  $parcelsIdsModify.=$row['id'];
  $parcelsIdsModify.='</option>';

}
$parcelsIdsModify.='</select>';
$parcelsIds.='</select>';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="css/leaflet.css"/>
    <link href="font-awesome/css/all.css" rel="stylesheet"> <!--load all styles -->
      <!-- add sidebar css -->
   <link rel="stylesheet" href="css/leaflet-sidebar.css" />
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/map.css">
   <script defer src="font-awesome/js/all.js"></script> <!--load all styles -->
   <script src="js/jquery-3.4.1.min.js"></script>

    <link rel="icon" href="data/pictures/favicon.ico" />
    <title>Ferme de Kervel</title>
</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top" role="navigation">
      <a class="navbar-brand" href="#">Ferme de Kervel</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">

        <ul class="nav navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="dashboard.php">Tableau de bord</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logout.php">Déconnection</a>
          </li>
         
        </ul>
      </div>
    </nav>

<div id="sidebar" class="sidebar collapsed">
        <!-- Nav tabs -->
        <div class="sidebar-tabs">
            <ul role="tablist">
                <li><a href="#home" role="tab"><span data-feather="align-justify"></a></li>
                <li><a href="#Infos" role="tab"><span data-feather="info"></i></a></li>              
                <li><a href="#events" role="tab"><span data-feather="zap"></i></a></li>
                <li><a href="#export" role="tab"><span data-feather="file-text"></i></a></li>               
            </ul>

            <ul role="tablist">
                <li><a href="#settings" role="tab"><span data-feather="settings"></a></li>
            </ul>
        </div>

        <!-- Tab panes -->
        <div class="sidebar-content">
            <div class="sidebar-pane" id="home">
                <h1 class="sidebar-header">
                Tableau de bord
                    <span class="sidebar-close"><i class="fa fa-caret-left"></i></span>
                </h1>

                <br>
                <div id="waitIndicatorModifyParcel" style="display:none;" class="spinner-border spinner-border-sm text-primary float-right mr-2" role="status">  
                </div> 
                <h5>Modifier un parcelle</h5>

                <hr class="mb-4" />
                <form id="editParcelForm" name="editParcelForm" role="form">
                  <!-- id parcel -->
                  <label for="parcels">
                  Choisissez une parcelle <span class="text-muted"></span></label>
                  <div class="mb-3">
                  <?php echo($parcelsIdsModify); ?>

                  </div>

                  <!-- Parcel Commune -->
                  <div class="mb-3">
                    <label for="editParcelCommune">
                    Commune <span class="text-muted"></span>
                    </label>

                    <input
                      type="text"
                      class="form-control"
                      id="editParcelCommune"
                      name="editParcelCommune"
                    />
                  </div>

                  <!-- Parcel Adresse -->
                  <div class="mb-3">
                    <label for="editParcelAdresse">
                    Adresse <span class="text-muted"></span>
                    </label>
                    
                    <input
                      type="text"
                      class="form-control"
                      id="editParcelAdresse"
                      name="editParcelAdresse"
                    />
                  </div>

                  <!-- Code cadastre -->
                  <div class="mb-3">
                    <label for="editParcelCodeCadastre">
                    Code cadastre <span class="text-muted"></span>
                    </label>
                    
                    <input
                      type="text"
                      class="form-control"
                      id="editParcelCodeCadastre"
                      name="editParcelCodeCadastre"
                    />
                  </div>

                  <!-- Numéro cadastre -->
                  <div class="mb-3">
                    <label for="editParcelNumCadastre">
                    Numéro cadastre <span class="text-muted"></span>
                    </label>
                    
                    <input
                      type="text"
                      class="form-control"
                      id="editParcelNumCadastre"
                      name="editParcelNumCadastre"
                    />
                  </div>

                  <!-- Montant achat -->
                  <div class="mb-3">
                    <label for="editParcelMontantAchat">
                    Montant achat <span class="text-muted"></span>
                    </label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupPrepend">€</span>
                      </div>
                      <input 
                      type="text" 
                      class="form-control" 
                      id="editParcelMontantAchat" 
                      name="editParcelMontantAchat" 
                      aria-describedby="inputGroupPrepend"
                      />
                    </div>
                  </div>

                  <!-- Montant Loyer -->
                  <div class="mb-3">
                    <label for="editParcelMontantLoyer">
                    Montant loyer <span class="text-muted"></span>
                    </label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupPrepend">€</span>
                      </div>
                      <input 
                      type="text" 
                      class="form-control" 
                      id="editParcelMontantLoyer" 
                      name="editParcelMontantLoyer" 
                      aria-describedby="inputGroupPrepend"
                      />
                    </div>
                  </div>

                  <!-- date d’autorisation à exploiter -->
                  <div class="mb-3">
                  <label for="editParcelAutorisationDate">
                  Date d’autorisation à exploiter <span class="text-muted"></span></label>

                  <input
                    type="date"
                    class="form-control"
                    id="editParcelAutorisationDate"
                    name="editParcelAutorisationDate"
                  />
                  </div>

                  <!-- relevé MSA -->
                  <div class="mb-3">
                    <label for="editParcelMSA">
                    Relevé MSA <span class="text-muted"></span></label><br/>

                    <div class="form-check-inline">
                      <input class="form-check-input" type="radio" name="editParcelMSA" id="editParcelMSARadio1" value="oui">
                      <label class="form-check-label font-italic" for="editParcelMSARadio1">Oui</label>
                    </div>
                    <div class="form-check-inline">
                      <input class="form-check-input" type="radio" name="editParcelMSA" id="editParcelMSARadio2" value="non">
                      <label class="form-check-label font-italic" for="editParcelMSARadio2">Non</label>
                    </div>
                  </div>

                    <!-- Code MAE -->
                    <div class="mb-3">
                    <label for="editParcelCodeMae">
                    Code MAE <span class="text-muted"></span></label><br/>

                    <div class="form-check-inline">
                      <input class="form-check-input" type="radio" name="editParcelCodeMae" id="editParcelCodeMaeRadio1" value="SPE12">
                      <label class="form-check-label font-italic" for="editParcelCodeMaeRadio1">SPE12</label>
                    </div>
                    <div class="form-check-inline">
                      <input class="form-check-input" type="radio" name="editParcelCodeMae" id="editParcelCodeMaeRadio2" value="bocage">
                      <label class="form-check-label font-italic" for="editParcelCodeMaeRadio2">bocage</label>
                    </div>
                  </div>

                  <!-- Type de parcelle -->
                  <div class="mb-3">
                    <label for="editParcelMSA">
                    Type de parcelle <span class="text-muted"></span></label><br/>

                    <div class="form-check-inline">
                      <input class="form-check-input" type="radio" name="editParcelType" id="editParcelTypeRadio1" value="prairie naturelle">
                      <label class="form-check-label font-italic" for="editParcelTypeRadio1">Prairie naturelle</label>
                    </div>
                    <div class="form-check-inline">
                      <input class="form-check-input" type="radio" name="editParcelType" id="editParcelTypeRadio2" value="prairie humide">
                      <label class="form-check-label font-italic" for="editParcelTypeRadio2">Prairie humide</label>
                    </div>
                  </div>

                   <!-- Nom du propriétaire -->
                   <div class="mb-3">
                    <label for="editParcelNomProprietaire">
                    Nom du propriétaire <span class="text-muted"></span>
                    </label>
                    
                    <input
                      type="text"
                      class="form-control"
                      id="editParcelNomProprietaire"
                      name="editParcelNomProprietaire"
                    />
                  </div>

                   <!-- Nom locataire -->
                   <div class="mb-3">
                    <label for="editParcelNomLocataire">
                    Nom locataire <span class="text-muted"></span>
                    </label>
                    
                    <input
                      type="text"
                      class="form-control"
                      id="editParcelNomLocataire"
                      name="editParcelNomLocataire"
                    />
                  </div>

                    <!-- Demande Pac -->
                    <div class="mb-3">
                    <label for="editParcelMSA">
                    Demande PAC <span class="text-muted"></span></label><br/>

                    <div class="form-check-inline">
                      <input class="form-check-input" type="radio" name="editParcelDemandePac" id="editParcelDemandePacRadio1" value="oui">
                      <label class="form-check-label font-italic" for="editParcelDemandePacRadio1">Oui</label>
                    </div>
                    <div class="form-check-inline">
                      <input class="form-check-input" type="radio" name="editParcelDemandePac" id="editParcelDemandePacRadio2" value="non">
                      <label class="form-check-label font-italic" for="editParcelDemandePacRadio2">Non</label>
                    </div>
                  </div>

                  <!-- types d'animaux -->
                  <div class="mb-3">
                    <label for="editParcelMSA">
                    Types d'animaux <span class="text-muted"></span></label><br/>

                    <div class="form-check-inline">
                      <input class="form-check-input" type="radio" name="editParcelTypeAnimaux" id="editParcelTypeAnimauxRadio1" value="vache">
                      <label class="form-check-label font-italic" for="editParcelTypeAnimauxRadio1">vache</label>
                    </div>
                    <div class="form-check-inline">
                      <input class="form-check-input" type="radio" name="editParcelTypeAnimaux" id="editParcelTypeAnimauxRadio2" value="genisse">
                      <label class="form-check-label font-italic" for="editParcelTypeAnimauxRadio2">génisse</label>
                    </div>
                  </div>

                   <!-- Nombre Animaux -->
                   <div class="mb-3">
                    <label for="editParcelNombreAnimaux">
                    Nombre d'animaux <span class="text-muted"></span>
                    </label>
                    
                    <input
                      type="text"
                      class="form-control"
                      id="editParcelNombreAnimaux"
                      name="editParcelNombreAnimaux"
                    />
                  </div>

                  <!-- nom du fichier shapefile -->
                  <div class="mb-3">
                    <label for="editParcelNomShapefile">
                    Nom du fichier Shapefile <span class="text-muted"></span>
                    </label>
                    
                    <input
                      type="text"
                      class="form-control"
                      id="editParcelNomShapefile"
                      name="editParcelNomShapefile"
                    />
                  </div>

                  <br>
                  <p class="text-success float-left" style="display:none;" id="parcelEditOkMsg">Parcelle modifiée avec succès</p>
                  <p class="text-danger float-left" style="display:none;" id="parcelEditFailMsg">Erreur</p>
                  <!-- Button trigger modal -->
                  <button type="submit" id="editParcelFormBtn" class="btn btn-primary mb-3 mr-3 float-right">
                  Soumettre
                  </button>  
                </form>
                <br>
           

            </div>

            <div class="sidebar-pane" id="Infos">
                <h1 class="sidebar-header">Légende de carte<span class="sidebar-close"><i class="fa fa-caret-left"></i></span></h1>
                <br>
                <h5>Colorez les parcelles en fonction de:</h5>
                <hr class="mb-4" />
                <input type="radio" name="colorParcelles" value="code_mae" id="areaRadio"> Code MAE<br>
                <input type="radio" name="colorParcelles" value="type" id="typeRadio"> Type<br>
                <input type="radio" name="colorParcelles" value="type_animaux" id="valueRadio"> Type animaux<br>

            </div>

            <div class="sidebar-pane" id="events">
                <h1 class="sidebar-header">Événements<span class="sidebar-close"><i class="fa fa-caret-left"></i></span></h1>
                <br>
                <h5>Choisissez un numéro de parcelle:</h5>
                <?php echo($parcelsIds); ?>

                <br>
                <br>

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" id="showEventsBtn">
                Afficher les événements
                </button> 
                <button type="button" class="btn btn-success" id="addEventBtn">
                Ajouter 
                </button> 
                <div id="waitIndicatorEvent" style="display:none;" class="spinner-border spinner-border-sm text-primary" role="status">  <span class="sr-only">Loading...</span></div> 
                <br>
                <br>
                <!-- event table -->
 <!-- table -->

 <div class="table-responsive">
 <table class="table table-hover" id="eventsTable">
  <thead>
    <tr>
      <th scope="col" class="d-none">#</th>
      <th scope="col" class="d-none">id parcelle</th>
      <th scope="col">Type</th>
      <th scope="col">Paramètre</th>
      <th scope="col">Date</th>
      <th scope="col"></th>
      <th scope="col"></th>

    </tr>
  </thead>
  <tbody id="eventsBody">
<!-- events here -->
  </tbody>
</table>
      </div>
            </div>

<!-- export tab -->
            <div class="sidebar-pane" id="export">
                <h1 class="sidebar-header">Exportation<span class="sidebar-close"><i class="fa fa-caret-left"></i></span></h1>
                <br>
                <h5>Exporter des données</h5>
                <hr class="mb-4" />

                <!-- parcels ids -->
                <div class="mb-3">
                <label for="parcels">
                Choisis un parcel <span class="text-muted"></span></label>

                <?php echo($parcelsIds); ?>

                </div>

                 <!-- startDate -->
                 <div class="mb-3">
                <label for="startDate">
                Date de début <span class="text-muted"></span></label>

                <input
                  type="datetime-local"
                  class="form-control"
                  id="startDate"
                  name="startDate"
                  disabled
                />
                </div>

                 <!-- endDate -->
                 <div class="mb-3">
                <label for="endDate">
                Date de fin <span class="text-muted"></span></label>

                <input
                  type="datetime-local"
                  class="form-control"
                  id="endDate"
                  name="endDate"
                  disabled
                />
                </div>

                 <!-- format -->
                 <div class="mb-3">
                <label for="format">
                Format <span class="text-muted"></span></label>

                 <select class="custom-select" id="format" disabled>
                <option value="1">PDF</option>
                <option value="2">CVS</option>
                <option value="3">DOC</option>

                </select> 
                </div>

                <br>
                <br>

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary float-right" disabled>
                Exporter
                </button>         
            </div>

<!-- settings tab -->
            <div class="sidebar-pane" id="settings">
                <h1 class="sidebar-header">Paramètres<span class="sidebar-close"><i class="fa fa-caret-left"></i></span></h1>
                <br>
                <h5>Configurer les codes couleurs</h5>
                <hr class="mb-4" />
                <p class="font-weight-bold">Types de parcelles</p>
                 <!-- Prairie naturelle -->
                 <div class="mb-3">
                  <input type="color" name="PrairieNaturelleColor" id="PrairieNaturelleColor" value="#ffff00" disabled>
                  <label for="PrairieNaturelleColor">Prairie naturelle <span class="text-muted"></span></label>
                </div>

                <!-- Prairie humide -->
                <div class="mb-3">
                  <input type="color" name="PrairieHumideColor" id="PrairieHumideColor" value="#ffa500" disabled>
                  <label for="PrairieHumideColor">Prairie humide <span class="text-muted"></span></label>
                </div>

                <hr class="mb-4" />
                <p class="font-weight-bold">Codes MAE</p>
                 <!-- SPE12 -->
                 <div class="mb-3">
                  <input type="color" name="SPE12Color" id="SPE12Color" value="#008080" disabled>
                  <label for="SPE12Color">SPE12 <span class="text-muted"></span></label>
                </div>

                <!-- bocage -->
                <div class="mb-3">
                  <input type="color" name="bocageColor" id="bocageColor" value="#45f5f1" disabled>
                  <label for="bocageColor">Bocage <span class="text-muted"></span></label>
                </div>

                <hr class="mb-4" />
                <p class="font-weight-bold">Types d’animaux</p>
                 <!-- vache -->
                 <div class="mb-3">
                  <input type="color" name="typeVColor" id="typeVColor" value="#ff0000" disabled>
                  <label for="typeVColor">vache <span class="text-muted"></span></label>
                </div>

                <!-- génisse -->
                <div class="mb-3">
                  <input type="color" name="typeGColor" id="typeGColor" value="#f8f640" disabled>
                  <label for="typeGColor">génisse <span class="text-muted"></span></label>
                </div>

             

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary float-right" disabled>
                Sauvgarder
                </button>  
            </div>
        </div>
    </div>

     <!-- edit modal start -->

<!-- Modal -->
<div class="modal fade" id="editEventModalCenter" tabindex="-1" role="dialog" aria-labelledby="editEventModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editEventModalCenterTitle">Modifier un événement (parcelle 1)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form id="editEventForm" name="addEventForm" role="form">
      <div class="modal-body">
       <!-- date -->
       <div class="mb-3">
                <label for="editEventDate">
                Date<span class="text-muted"></span></label>

                <input
                  type="date"
                  class="form-control"
                  id="editEventDate"
                  name="editEventDate"
                  required=""
                />
                </div>
        

         <!-- Type -->
         <div class="mb-3">
                <label for="editEventType">
                Type <span class="text-muted"></span></label>

                <select class="custom-select" name="editEventType" id="editEventType">
                <option value="fauche" selected>fauche</option>
                <option value="semis">semis</option>
                <option value="presse">presse</option>
                <option value="entrée">entrée</option>
                <option value="sortie">sortie</option>
                <option value="entrée">chaleur</option>
                <option value="sortie">saillie</option>
                <option value="perte">perte</option>
                <option value="naissance">naissance</option>
                </select> 

                </div>

                 <!-- param -->
                <div class="mb-3">
                  <label for="editEventParam">
                  Paramètres <span class="text-muted"></span></label>

                  <input
                  type="text"
                    class="form-control"
                    id="editEventParam"
                    name="editEventParam"
                  />
                </div>

                <div class="form-group">
						      <input type="hidden" name="editEventId" value="" id="editEventId" class="form-control">
					      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <input type="submit" value="Sauvgarder" class="btn btn-success" id="editEventSubmit">
      </div>
      </form>
    </div>
  </div>
</div>
     <!-- edit event modal ends -->

     
     <!-- add event modal start -->

<div class="modal fade" id="addEventModalCenter" tabindex="-1" role="dialog" aria-labelledby="addEventModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addEventModalCenterTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="addEventForm" name="addEventForm" role="form">
      <div class="modal-body">
       <!-- date -->
       <div class="mb-3">
                <label for="date">
                Date<span class="text-muted"></span></label>

                <input
                  type="date"
                  class="form-control"
                  id="date"
                  name="date"
                  required=""
                />
                </div>

        <!-- Type -->
        <div class="mb-3">
                <label for="event_type">
                Type <span class="text-muted"></span></label>

                <select class="custom-select" name="event_type" id="event_type">
                <option value="fauche" selected>fauche</option>
                <option value="semis">semis</option>
                <option value="presse">presse</option>
                <option value="entrée">entrée</option>
                <option value="sortie">sortie</option>
                <option value="entrée">chaleur</option>
                <option value="sortie">saillie</option>
                <option value="perte">perte</option>
                <option value="naissance">naissance</option>
                </select> 

                </div>

                 <!-- param -->
                 <div class="mb-3">
                <label for="event_param">
                Paramètres <span class="text-muted"></span></label>

                <input
                  type="text"
                  class="form-control"
                  id="event_param"
                  name="event_param"
                  disabled="disabled"
                />
                </div>

                <div class="form-group">
						      <input type="hidden" name="id" value="" id="id" class="form-control">
					      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <input type="submit" value="Soumettre" class="btn btn-success" id="submit">
      </div>
      </form>
    </div>
  </div>
</div>
     <!-- add event modal ends -->

 <!-- delete event confirm model starts -->     

 <div class="modal fade" id="deleteEventConfirmModal" tabindex="-1" role="dialog" aria-labelledby="deleteEventConfirmModalTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteEventConfirmModalTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Êtes-vous sûr?</p>
        <form id="deleteEventForm" name="deleteEventForm" role="form">
          <input type="hidden" name="deleteEventIdInput" value="" id="deleteEventIdInput" class="form-control">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Non</button>
        <button type="submit" class="btn btn-primary">Oui</button>
      </div>
      </form>
    </div>
  </div>
</div>
 <!-- delete event confirm model ends -->     

<div id="container">
    <div class="row justify-content-center">    
        <div class="col-md-12">
            <div id="mapid" class="sidebar-map"></div>
        </div>
    </div>
 
    

<!-- Make sure you put this AFTER Leaflet's CSS (v1.5.1) -->
<script src="js/leaflet.js"></script>

<script src="data/leaflet.ajax.min.js"></script>
<script src="js/leaflet-sidebar.js"></script>
<script src="js/map.js"></script>


<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- Icons -->
<script src="js/feather.min.js"></script>
<script>
    feather.replace();
</script>
  
<script src="js/events.js"></script>

</body>

</html>