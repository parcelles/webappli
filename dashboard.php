<?php
require_once("data/session.php"); 
require_once("data/db_connection.php"); 
require_once("data/functions.php"); 


//redirect if not auth
if(!isset($_SESSION['idAdmin'])) SendToLogin();

$str="";

if(isset($_POST['username']) && isset($_POST['email']) && isset($_POST['firstName']) && isset($_POST['lastName'])){

  $username=pg_escape_string($connection, $_POST['username']);
  $email=pg_escape_string($connection, $_POST['email']);
  $firstName=pg_escape_string($connection, $_POST['firstName']);
  $lastName=pg_escape_string($connection, $_POST['lastName']);
  $id="";
  if(isset($_SESSION["idAdmin"])) $id= $_SESSION["idAdmin"];

  //get user by username
	$result = GetUserById($id);
	if(!$result){
		die("query error");
	}

    $row=pg_fetch_assoc($result);
    if($row){

		//check if empty
            if($username != "" && $email != ""){
                EditUserInfo($id,$username,$email,$firstName,$lastName);
                $str='<p class="label label-sm label-success">Vos données ont été modifiées avec succès</p>';
            }

	}
    else{
		$str='<p class="label label-sm label-danger">Query error</p>';
    }
}

//fill fields
$usernameAdmin="";
$emailAdmin="";
$firstNameAdmin="";
$lastNameAdmin="";

if(isset($_SESSION['idAdmin'])){
  $id= $_SESSION["idAdmin"];

  //get user by username
  $result = GetUserById($id);

  $row=pg_fetch_assoc($result);
  if($row){
    $usernameAdmin=$row['username'];
    $emailAdmin=$row['email'];
    $firstNameAdmin=$row['first_name'];
    $lastNameAdmin=$row['last_name'];
  }
}


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="data/pictures/favicon.ico" />

    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet" />
  </head>

  <body>
    <!-- navbar -->
    <?php include 'nav.php'; ?>

    <div class="container-fluid">
      <div class="row">
        <!-- sidebar -->
        <?php include 'sidebar.php'; ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"
          >
            <h1 class="h2">Données personnelles</h1>
          </div>

          <div class="col-md-8 order-md-1">
            <form class="needs-validation" novalidate="" action="dashboard.php" method="POST" role="form">
            <!-- username -->
              <div class="mb-3">
                <label for="username">Nom d'utilisateur</label>
 
                  <input
                    type="text"
                    class="form-control"
                    id="username"
                    name="username"
                    placeholder="Username"
                    value="<?php echo $usernameAdmin;?>"
                    required=""
                  />
                  <div class="invalid-feedback" style="width: 100%;">
                  Votre nom d'utilisateur ne doit pas être vide.
                  </div>
                </div> 

            <!-- email -->
              <div class="mb-3">
                <label for="email"
                  >Email</label
                >
                <input
                  type="email"
                  class="form-control"
                  id="email"
                  name="email"
                  placeholder="you@example.com"
                  value="<?php echo $emailAdmin;?>"
                  required=""
                />
                <div class="invalid-feedback">
                S'il vous plaît, mettez une adresse email valide.
                </div>
              </div>

              
            <!-- first name -->
            <div class="mb-3">
                <label for="firstName"
                  >Prénom</label
                >
                <input
                  type="text"
                  class="form-control"
                  id="firstName"
                  name="firstName"
                  value="<?php echo $firstNameAdmin;?>"
                  required=""
                />
                <div class="invalid-feedback">
                Votre prénom ne doit pas être vide.
                </div>
              </div>

              
            <!-- last name -->
            <div class="mb-3">
                <label for="lastName"
                  >Nom</label
                >
                <input
                  type="text"
                  class="form-control"
                  id="lastName"
                  name="lastName"
                  value="<?php echo $lastNameAdmin;?>"
                  required=""
                />
                <div class="invalid-feedback">
                Votre nom ne doit pas être vide.
                </div>
              </div>


              <div class="form-group row">
												<div class="col-md-5 offset-3">
													<?php echo $str;?>
												</div>
											</div>


              <hr class="mb-4" />
              <button class="btn btn-primary btn-lg" type="submit">
                Sauvgarder
              </button>
            </form>
            <br />
          </div>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="css/bootstrap.min.css"></script>

    <!-- Icons -->
    <script src="js/feather.min.js"></script>
    <script>
      feather.replace();
    </script>

        <!-- form validation -->
<script src="js/validator.js"></script>
</body>
</html>
