--
-- PostgreSQL database dump
--

-- Dumped from database version 12.0
-- Dumped by pg_dump version 12.0

-- Started on 2020-04-06 02:24:14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2 (class 3079 OID 18999)
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- TOC entry 3741 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 214 (class 1259 OID 28671)
-- Name: couleurs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.couleurs (
    id integer NOT NULL,
    key character varying(32),
    value character varying(32)
);


ALTER TABLE public.couleurs OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 28669)
-- Name: couleurs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.couleurs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.couleurs_id_seq OWNER TO postgres;

--
-- TOC entry 3742 (class 0 OID 0)
-- Dependencies: 213
-- Name: couleurs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.couleurs_id_seq OWNED BY public.couleurs.id;


--
-- TOC entry 216 (class 1259 OID 28679)
-- Name: evenements; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.evenements (
    id integer NOT NULL,
    date date,
    type text,
    parameter text,
    id_parcelle integer
);


ALTER TABLE public.evenements OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 28677)
-- Name: evenements_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.evenements_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evenements_id_seq OWNER TO postgres;

--
-- TOC entry 3743 (class 0 OID 0)
-- Dependencies: 215
-- Name: evenements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.evenements_id_seq OWNED BY public.evenements.id;


--
-- TOC entry 218 (class 1259 OID 28687)
-- Name: log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.log (
    id integer NOT NULL,
    username character varying(60),
    date date,
    type_operation character varying(60)
);


ALTER TABLE public.log OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 28685)
-- Name: log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.log_id_seq OWNER TO postgres;

--
-- TOC entry 3744 (class 0 OID 0)
-- Dependencies: 217
-- Name: log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.log_id_seq OWNED BY public.log.id;


--
-- TOC entry 219 (class 1259 OID 28747)
-- Name: parcelle_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.parcelle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.parcelle_id_seq OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 28645)
-- Name: parcelles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parcelles (
    id integer DEFAULT nextval('public.parcelle_id_seq'::regclass) NOT NULL,
    commune text,
    adresse text,
    code_cadastre text,
    numero_cadastre text,
    geom public.geometry(MultiPolygon,4326),
    surface text,
    montant_achat text,
    date_autorisation date,
    releve_msa text,
    nom_shapefile text,
    code_mae text,
    type text,
    nom_proprietaire text,
    nom_locataire text,
    demande_pac text,
    type_animaux text,
    nombre_animaux integer,
    montant_loyer text,
    fid double precision
);


ALTER TABLE public.parcelles OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 28663)
-- Name: policies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.policies (
    id integer NOT NULL,
    name character varying(30)
);


ALTER TABLE public.policies OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 28661)
-- Name: policies_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.policies_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.policies_id_seq OWNER TO postgres;

--
-- TOC entry 3745 (class 0 OID 0)
-- Dependencies: 211
-- Name: policies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.policies_id_seq OWNED BY public.policies.id;


--
-- TOC entry 209 (class 1259 OID 20392)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id smallint NOT NULL,
    username character varying(60) NOT NULL,
    email character varying(60),
    signup_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    password character varying(128) NOT NULL,
    user_type integer NOT NULL,
    last_name character varying(60),
    first_name character varying(60)
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 20390)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 3746 (class 0 OID 0)
-- Dependencies: 208
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 3574 (class 2604 OID 28674)
-- Name: couleurs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.couleurs ALTER COLUMN id SET DEFAULT nextval('public.couleurs_id_seq'::regclass);


--
-- TOC entry 3575 (class 2604 OID 28682)
-- Name: evenements id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evenements ALTER COLUMN id SET DEFAULT nextval('public.evenements_id_seq'::regclass);


--
-- TOC entry 3576 (class 2604 OID 28690)
-- Name: log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.log ALTER COLUMN id SET DEFAULT nextval('public.log_id_seq'::regclass);


--
-- TOC entry 3573 (class 2604 OID 28666)
-- Name: policies id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.policies ALTER COLUMN id SET DEFAULT nextval('public.policies_id_seq'::regclass);


--
-- TOC entry 3570 (class 2604 OID 20395)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 3730 (class 0 OID 28671)
-- Dependencies: 214
-- Data for Name: couleurs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.couleurs (id, key, value) FROM stdin;
\.


--
-- TOC entry 3732 (class 0 OID 28679)
-- Dependencies: 216
-- Data for Name: evenements; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.evenements (id, date, type, parameter, id_parcelle) FROM stdin;
\.


--
-- TOC entry 3734 (class 0 OID 28687)
-- Dependencies: 218
-- Data for Name: log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.log (id, username, date, type_operation) FROM stdin;
\.


--
-- TOC entry 3726 (class 0 OID 28645)
-- Dependencies: 210
-- Data for Name: parcelles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.parcelles (id, commune, adresse, code_cadastre, numero_cadastre, geom, surface, montant_achat, date_autorisation, releve_msa, nom_shapefile, code_mae, type, nom_proprietaire, nom_locataire, demande_pac, type_animaux, nombre_animaux, montant_loyer, fid) FROM stdin;
\.


--
-- TOC entry 3728 (class 0 OID 28663)
-- Dependencies: 212
-- Data for Name: policies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.policies (id, name) FROM stdin;
\.


--
-- TOC entry 3568 (class 0 OID 19304)
-- Dependencies: 204
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- TOC entry 3725 (class 0 OID 20392)
-- Dependencies: 209
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, username, email, signup_date, password, user_type, last_name, first_name) FROM stdin;
1	admin	admin@gmail.com	2019-12-19 02:08:23.097479+01	$2y$10$Wi7ufk84oDFdzFKKF1N.RebklJmUcemQNzI5wkT2Y9VVgtl8WmGJm	1	Andy	Doe
\.


--
-- TOC entry 3747 (class 0 OID 0)
-- Dependencies: 213
-- Name: couleurs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.couleurs_id_seq', 1, false);


--
-- TOC entry 3748 (class 0 OID 0)
-- Dependencies: 215
-- Name: evenements_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.evenements_id_seq', 1, true);


--
-- TOC entry 3749 (class 0 OID 0)
-- Dependencies: 217
-- Name: log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.log_id_seq', 1, false);


--
-- TOC entry 3750 (class 0 OID 0)
-- Dependencies: 219
-- Name: parcelle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.parcelle_id_seq', 1, true);


--
-- TOC entry 3751 (class 0 OID 0)
-- Dependencies: 211
-- Name: policies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.policies_id_seq', 1, false);


--
-- TOC entry 3752 (class 0 OID 0)
-- Dependencies: 208
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


--
-- TOC entry 3588 (class 2606 OID 28676)
-- Name: couleurs couleurs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.couleurs
    ADD CONSTRAINT couleurs_pkey PRIMARY KEY (id);


--
-- TOC entry 3590 (class 2606 OID 28684)
-- Name: evenements evenements_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evenements
    ADD CONSTRAINT evenements_pkey PRIMARY KEY (id);


--
-- TOC entry 3592 (class 2606 OID 28692)
-- Name: log log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.log
    ADD CONSTRAINT log_pkey PRIMARY KEY (id);


--
-- TOC entry 3584 (class 2606 OID 28738)
-- Name: parcelles parcelles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parcelles
    ADD CONSTRAINT parcelles_pkey PRIMARY KEY (id);


--
-- TOC entry 3586 (class 2606 OID 28668)
-- Name: policies policies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.policies
    ADD CONSTRAINT policies_pkey PRIMARY KEY (id);


--
-- TOC entry 3580 (class 2606 OID 20398)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 3581 (class 1259 OID 28654)
-- Name: parcelles_geom_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX parcelles_geom_idx ON public.parcelles USING gist (geom);


--
-- TOC entry 3582 (class 1259 OID 28863)
-- Name: parcelles_geom_idx1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX parcelles_geom_idx1 ON public.parcelles USING gist (geom);


-- Completed on 2020-04-06 02:24:15

--
-- PostgreSQL database dump complete
--

