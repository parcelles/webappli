ALTER TABLE public.parcelles
    ALTER COLUMN commune TYPE text  COLLATE pg_catalog."default";

ALTER TABLE public.parcelles
    ALTER COLUMN adresse TYPE text  COLLATE pg_catalog."default";

ALTER TABLE public.parcelles
    ALTER COLUMN code_cadastre TYPE text  COLLATE pg_catalog."default";

ALTER TABLE public.parcelles
    ALTER COLUMN numero_cadastre TYPE text  COLLATE pg_catalog."default";

ALTER TABLE public.parcelles
    ALTER COLUMN surface TYPE text  COLLATE pg_catalog."default";

ALTER TABLE public.parcelles
    ALTER COLUMN montant_achat TYPE text  COLLATE pg_catalog."default";

ALTER TABLE public.parcelles
    ALTER COLUMN releve_msa TYPE text  COLLATE pg_catalog."default";

ALTER TABLE public.parcelles
    ALTER COLUMN code_mae TYPE text  COLLATE pg_catalog."default";

ALTER TABLE public.parcelles
    ALTER COLUMN type TYPE text  COLLATE pg_catalog."default";

ALTER TABLE public.parcelles
    ALTER COLUMN nom_proprietaire TYPE text  COLLATE pg_catalog."default";

ALTER TABLE public.parcelles
    ALTER COLUMN nom_locataire TYPE text  COLLATE pg_catalog."default";

ALTER TABLE public.parcelles
    ALTER COLUMN type_animaux TYPE text  COLLATE pg_catalog."default";

ALTER TABLE public.parcelles
    ALTER COLUMN montant_loyer TYPE text  COLLATE pg_catalog."default";

ALTER TABLE public.evenements
    ALTER COLUMN type TYPE text  COLLATE pg_catalog."default";

ALTER TABLE public.evenements
    ALTER COLUMN parameter TYPE text  COLLATE pg_catalog."default";

        