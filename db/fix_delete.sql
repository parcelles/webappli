 DROP TABLE IF EXISTS public.parcelles;
--
-- TOC entry 210 (class 1259 OID 28645)
-- Name: parcelles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parcelles (
    id integer DEFAULT nextval('public.parcelle_id_seq'::regclass) NOT NULL,
    commune character varying(5),
    adresse character varying(3),
    code_cadastre character varying(2),
    numero_cadastre character varying(4),
    geom public.geometry(MultiPolygon,4326),
    surface character varying(9),
    montant_achat character varying(16),
    date_autorisation date,
    releve_msa character varying(5),
    nom_shapefile character varying(64),
    code_mae character varying(20),
    type character varying(60),
    nom_proprietaire character varying(30),
    nom_locataire character varying(60),
    demande_pac character varying(5),
    type_animaux character varying(20),
    nombre_animaux integer,
    montant_loyer character varying(20),
    fid double precision
);


ALTER TABLE public.parcelles OWNER TO postgres;

--
-- TOC entry 3584 (class 2606 OID 28738)
-- Name: parcelles parcelles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parcelles
    ADD CONSTRAINT parcelles_pkey PRIMARY KEY (id);



--
-- TOC entry 3581 (class 1259 OID 28654)
-- Name: parcelles_geom_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX parcelles_geom_idx ON public.parcelles USING gist (geom);


--
-- TOC entry 3582 (class 1259 OID 28863)
-- Name: parcelles_geom_idx1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX parcelles_geom_idx1 ON public.parcelles USING gist (geom);
